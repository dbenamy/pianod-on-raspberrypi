#!/bin/bash

sudo mkdir /package/flipit
sudo chown pi:pi /package/flipit
(
	cd /package/flipit
	curl -q https://flipit.googlecode.com/files/flipit-0.3.6.tar.gz | tar xvz --strip=1
	./configure
	make
)
sudo ln -s /package/flipit/flipit /command/
#sudo flipit -t /dev/ttyUSB0 flip a3 on # test

sudo ln -s "$(pwd)/x10/go" /package/http-flipit
sudo apt-get install --yes apache2 golang
# sudo ln -s "$(pwd)/http-flipit" /command/
# sudo ./http-flipit
sudo mkdir -p /var/svc.d/http-flipit/log
sudo mkdir -p /var/log/http-flipit
sudo chown multilog:multilog /var/log/http-flipit
sudo ln -s /var/log/http-flipit/current /var/log/http-flipit.log
echo '#!/bin/sh
exec 2>&1
exec setuidgid multilog multilog t s10485760 n2 /var/log/http-flipit
' | sudo tee /var/svc.d/http-flipit/log/run
sudo chmod +x /var/svc.d/http-flipit/log/run
echo '#!/bin/sh
exec 2>&1
export GOARM=5
export GOOS=linux
export GOARCH=arm
exec go run /package/http-flipit/http-flipit.go
' | sudo tee /var/svc.d/http-flipit/run
sudo chmod +x /var/svc.d/http-flipit/run
sudo ln -s /var/svc.d/http-flipit /etc/service/

sudo mv /var/www /var/www.orig
sudo ln -s "$(pwd)/x10/static" /var/www
