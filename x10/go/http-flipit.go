package main

import (
	"fmt"
	"net/http"
	"os/exec"
)

func handler(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
	var unit string = r.FormValue("unit")
	var op string = r.FormValue("op")
	UNITS := map[string]bool {
		"1": true,
		"2": true,
		"3": true,
		"4": true,
		"5": true,
		"6": true,
		"7": true,
		"8": true,
	}
	if UNITS[unit] && (op == "on" || op == "off") {
		cmd := exec.Command("/command/flipit", "-t", "/dev/ttyUSB0", "flip", "a" + unit, op)
		cmd.Run()
		fmt.Fprintf(w, "Done.")
	}
}

func main() {
	fmt.Printf("Starting up.\n")
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
