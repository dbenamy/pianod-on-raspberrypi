#!/bin/bash

set -eux

SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) # http://stackoverflow.com/a/246128/229371

apt-get --yes update
apt-get --yes upgrade
apt-get --yes install libao-dev libgcrypt11-dev libgnutls-dev libjson0-dev libfaad-dev libmad0-dev libbsd-dev daemontools-run \
                      m4 # for web client

# pulse audio really necessary?
#apt-get --yes install pulseaudio

# need this if running pianod from init.d as root via alsa
usermod -a -G audio root
# need this if using pulse audio and running as nobody
usermod -G pulse-access nobody

# install the web client
cd /tmp
curl http://deviousfish.com/Downloads/pianod/Devel/pianod-client-50.tar.gz | tar xvz
cd pianod-client-50
sed -i 's/^TIDY *=.*$/TIDY = cat/' Makefile # the html causes tidy warnings and stops the build
make
mkdir /opt/pianod-client
cp *.html *.css *.js *.jpeg *.gif /opt/pianod-client
chown -R nobody /opt/pianod-client # because we run pianod as nobody & it's serving this
rm -rf /tml/pianod-client-50

# install pianod
cd /tmp
wget http://deviousfish.com/Downloads/pianod/pianod-165.tar.gz
tar xfz pianod-165.tar.gz
cd pianod-165
# libbsd supplies get/setprogname. Linking version 165 fails without this.
LIBS=-lbsd ./configure
make
make install
cd /tmp
rm -rf pianod-165 pianod-165.tar.gz
cp "$SCRIPTDIR/pianod.startscript" /etc/pianod.startscript

# Run pianod via daemontools to make it easy to get logging output & because I like it :-)
mkdir -p /etc/service/pianod/log
mkdir -p /var/log/pianod
ln -s /var/log/pianod/current /var/log/pianod.log
echo '#!/bin/sh
exec 2>&1
exec multilog t s10485760 n2 /var/log/pianod
' > /etc/service/pianod/log/run
chmod +x /etc/service/pianod/log/run
echo '#!/bin/sh
exec 2>&1
exec /usr/local/bin/pianod -i /etc/pianod.startscript -u /etc/pianod.passwd -c /opt/pianod-client -Z 0xfefd
' > /etc/service/pianod/run
chmod +x /etc/service/pianod/run

# install wsgw
cd /tmp
wget http://deviousfish.com/Downloads/wsgw/wsgw-24.tar.gz
tar xfz wsgw-24.tar.gz
cd wsgw-24
./configure
make
make install
cd /tmp
rm -rf wsgw-24 wsgw-24.tar.gz

# install extra files
cd "$SCRIPTDIR"
cp wsgw.conf /etc/wsgw.conf
cp wsgw.init /etc/init.d/wsgw
chmod a+x /etc/init.d/wsgw

# run at startup
update-rc.d wsgw defaults

echo ""
echo "Almost done!"
echo ""
echo "You'll need to edit /etc/pianod.startscript to set your pandora email address and password, and then run `/etc/init.d/pianod start`."
echo ""
echo "Then you should be able to go to http://pi.local:4446 if you're on an Apple device or something with mdns set up. Elsewhere, replace `pi.local` in the url with your pi's ip address."
