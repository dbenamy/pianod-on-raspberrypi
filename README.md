# Intro

This repo has installers which make it easy to set up a Raspberry Pi for useful home control things:

- a Pandora radio
- an Apple Airplay receiver so Apple devices can stream audio to it
- an X10 controller, via the CM17A Firecracker

TODO rename the repo since I've generalized it beyond its original purpose.

It isn't a very secure setup so if you copy it, I wouldn't put any sensitive info on it, or use it to control anything really important.

Some of this is really hacky and specific to my setup.


# Basic Raspberry Pi Setup

1. Download noobs, unzip it, copy files (not top-level folder) to an sd card, at least 8gb, formatted with fat32.

2. Boot the raspberry pi. At the noobs menu, install raspbian.

3. At first boot it'll run raspi-config. Make sure to explicitly set to locale (under internationalization options). You probably want en_us.utf-8. I'm not sure if that's exactly it but it's something like that. Also set the keyboard layout, probably to Generic Keyboard & US. Without at least one of these, you won't be able to type "|" on a US keyboard.

4. When it's done, you should be logged in. If not: pi, raspberry.

5. If you're at a terminal, run X with `startx`.

6. Run the wifi program and connect to wifi. Make sure to save the connection.

7. Switch back to the terminal. One way to do that is `ctrl + alt + f1`, `ctrl + c`.

8. Rename the Hostname to "pi" so it's shorter to type later: `echo pi | sudo tee /etc/hostname && sudo /etc/init.d/hostname.sh start; bash`

9. Install avahi-daemon so some devices can automatically detect the raspberry pi: `sudo apt-get --yes update && sudo apt-get install --yes avahi-daemon`.

If you want, now you can go to a real computer and continue working via ssh:
```
$ ssh pi@pi.local # on apple devices and things with mdns. elsewhere you'll need the pi's ip address instead of `pi.local`.
```


# Daemontools

The djb way baby! http://thedjbway.b0llix.net/hier.html
```
apt-get --yes update
apt-get --yes install daemontools-run
ln -s /usr/local/bin/ /command
ln -s /opt /package
ln -s /etc/service /service
# mkdir /etc/tcprules # don't need this yet
useradd --shell /usr/sbin/nologin --system -M multilog
mkdir /var/multilog
chown multilog:multilog /var/multilog
mkdir /var/svc.d
```


# General Audio Setup

If you're using a usb audio device (for better quality sound), you'll need to set things up so that it's the default.
```
sudo nano /etc/modprobe.d/alsa-base.conf
# If there's a line starting with `options snd-usb-audio`, change its index to 0.
# If not, add a line `options snd-usb-audio index=0`.
# Add a line `options snd_bcm2835 index=1`.
sudo reboot # So settings take effect
aplay -l # Verify that the usb audio device is card 0
sudo aplay /usr/share/sounds/alsa/Front_Center.wav # Test alsa playback
alsamixer # To fiddle with the alsa volume
```


# Next

`git clone https://bitbucket.org/dbenamy/pianod-on-raspberrypi.git`

If you want to be able to send audio from airplay devices, like iphones and itunes on macs, you'll need shairport or shairport-sync.

If you also want to be able to stream pandora from within the raspberry pi, you'll need pianod. Warning: it's ugly and not quite complete and I had trouble getting it working. You don't need this to run pandora on your ios or osx device and stream it to the pi.

This repo only supports using shairplay* & pianod together by using pulseaudio and the no-longer-maintained shairport project (shairport-sync doesn't support pulseaudio). If you don't need to run pandora on the pi, I recommend going the shairport-sync route.

TODO It's probably possible to use hardware mixing to have multiple programs use alsa directly. I haven't tried yet.

In either case you can also run the x10 control stuff.


# Older shairport & pianod

Set up pulse audio so more than one program can "open" the sound device at once.
```
sudo apt-get --yes install pulseaudio pulseaudio-module-zeroconf libao-dev
sudo nano /etc/default/pulseaudio # Change `PULSEAUDIO_SYSTEM_START=1`
sudo /etc/init.d/pulseaudio stop
sudo /etc/init.d/pulseaudio start
sudo usermod -G pulse-access root # by default root can't access pulse audio. sigh.
sudo nano /etc/libao.conf # Change `default_driver=pulse`
sudo paplay /usr/share/sounds/alsa/Front_Center.wav # to test it
```

To send audio from airplay devices, like iphones and itunes on macs, install shairport: `sudo pianod-on-raspberrypi/shairport/pi-shairport2.sh`.

WIP: To use pandora from android or other devices without airplay, install pianod: `sudo pianod-on-raspberrypi/pianod/pi-pianod.sh`

TODO set up a visitor account that can play etc but not change pandora settings- http://wiki.deviousfish.com/index.php?title=Pianod_User_Setup, http://wiki.deviousfish.com/index.php?title=Pianod_Security


# Newer shairport-sync

If you've played with pulse audio, uninstall it first.

`./shairport-sync/install.sh`


# x10 Setup

This is super basic.

`./x10/install.sh`
