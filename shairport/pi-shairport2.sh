#!/bin/bash

set -eux

SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) # http://stackoverflow.com/a/246128/229371

apt-get --yes update
apt-get --yes upgrade
apt-get --yes install libao-dev libssl-dev libavahi-client-dev libasound2-dev

# configuration
cd "$SCRIPTDIR"
cp shairport.conf /etc/default/shairport
cp logrotate.shairport /etc/logrotate.d/shairport
usermod -G pulse-access pi # we'll run it as pi so give that use access to pulse audio

# shairport package
cd /tmp
git clone https://github.com/abrasive/shairport.git
cd shairport
./configure
PREFIX=/usr make install

# init script
cp scripts/debian/init.d/shairport /etc/init.d/shairport
chmod +x /etc/init.d/shairport
update-rc.d shairport defaults
/etc/init.d/shairport start

cd /tmp
rm -rf shairport
