#!/bin/bash

set -eux

# Set up system deps
sudo apt-get --yes install autoconf libtool libdaemon-dev libasound2-dev libpopt-dev avahi-daemon libavahi-client-dev libssl-dev
sudo useradd --shell /usr/sbin/nologin --system -M shairport
sudo usermod -G audio shairport # add shairport user to audio group so it can access alsa

# Install package
mkdir /package/shairport-sync
chown shairport:shairport /package/shairport-sync
sudo -u shairport bash <<"EOF"
git clone https://github.com/mikebrady/shairport-sync.git /package/shairport-sync/
cd /package/shairport-sync
git checkout 2.2
autoreconf -i -f
./configure --with-alsa --with-avahi --with-ssl=openssl
make
EOF

# Set up daemontools service
sudo mkdir -p /var/svc.d/shairport-sync/log
sudo mkdir -p /var/log/shairport-sync
sudo chown multilog:multilog /var/log/shairport-sync
sudo ln -s /var/log/shairport-sync/current /var/log/shairport-sync.log
echo '#!/bin/sh
exec 2>&1
exec setuidgid multilog multilog t s10485760 n2 /var/log/shairport-sync
' | sudo tee /var/svc.d/shairport-sync/log/run
sudo chmod +x /var/svc.d/shairport-sync/log/run
echo "#!/bin/sh
exec 2>&1
# Hrm. When run with 'setuidgid shairport', it fails to start up with 'failed to
# attach mixer'. Running the same command from a shell works fine. I don't have
# time to investigate further now.
#exec setuidgid shairport /package/shairport-sync/shairport-sync --name=AirPi -- -d hw:0 -t hardware -c PCM
exec /package/shairport-sync/shairport-sync --name=AirPi -- -d hw:0 -t hardware -c PCM
" | sudo tee /var/svc.d/shairport-sync/run
sudo chmod +x /var/svc.d/shairport-sync/run
sudo ln -s /var/svc.d/shairport-sync /etc/service/
